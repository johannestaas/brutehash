import sys
from itertools import product
from collections import namedtuple
from hashlib import (
    sha256 as hl_sha256,
    md5 as hl_md5,
    sha1 as hl_sha1,
)


def md5(data):
    return hl_md5(data).hexdigest()


def sha1(data):
    return hl_sha1(data).hexdigest()


def sha256(data):
    return hl_sha256(data).hexdigest()


Result = namedtuple('Result', ('data', 'hash', 'score'))


def rule_insert_bytes_at_seek(orig, seek=-1, valid_bytes=None, **kwargs):
    i = 0
    if not valid_bytes:
        valid_bytes = bytes(range(256))
    elif isinstance(valid_bytes, str):
        valid_bytes = valid_bytes.encode('utf8')

    if seek >= 0:
        def insert(data):
            return orig[:seek] + data + orig[seek:]
    elif seek == -1:
        def insert(data):
            return orig + data
    else:
        def insert(data):
            return orig[:seek + 1] + data + orig[seek + 1:]

    while True:
        i += 1
        print('Trying inserting {} byte'.format(i), file=sys.stderr)
        prod_items = [valid_bytes] * i
        for new in product(*prod_items):
            new = bytes(list(new))
            yield insert(new)


class Score:
    def __init__(self, score):
        self.score = score

    def __gt__(self, other):
        return self.score > other.score

    def __str__(self):
        return str(self.score)


def scoring_leftside(hash, desired_hash, **kwargs):
    score = 0
    for i, c in enumerate(hash):
        if c != desired_hash[i]:
            break
        score += 1
    return Score(score)


class BothSideScore:
    def __init__(self, left, right):
        self.left = left
        self.right = right

    def __gt__(self, other):
        ''' Only if both sides are greater or at least equal '''
        if (self.left, self.right) == (other.left, other.right):
            return False
        if self.left < other.left:
            return False
        if self.right < other.right:
            return False
        return True

    def __str__(self):
        return '({}, {})'.format(self.left, self.right)


def scoring_bothside(hash, desired_hash, **kwargs):
    left_score = scoring_leftside(hash, desired_hash).score
    right_score = scoring_leftside(hash[::-1], desired_hash[::-1]).score
    return BothSideScore(left_score, right_score)


def bruteforce(
    orig, desired_hash, hash_func=md5, rule_func=rule_insert_bytes_at_seek,
    scoring_func=scoring_leftside, output_path=None, **kwargs
):
    output_path = output_path or 'bruteforced_data.out'
    best_hash = Result(data=None, hash=None, score=None)
    try:
        for mutated in rule_func(orig, **kwargs):
            hash = hash_func(mutated)
            score = scoring_func(hash, desired_hash)
            if best_hash.score is None or score > best_hash.score:
                best_hash = Result(data=mutated, hash=hash, score=score)
                print('Found new data with score {}'.format(best_hash.score),
                      file=sys.stderr)
                print('compare desired: {}'.format(desired_hash),
                      file=sys.stderr)
                print('to new hash:     {}'.format(best_hash.hash),
                      file=sys.stderr)
    except KeyboardInterrupt:
        print('Quitting. Saving new data to {}'.format(output_path),
              file=sys.stderr)
        print('desired hash:  {}'.format(desired_hash))
        print('new hash:      {}'.format(best_hash.hash))
        with open(output_path, 'wb') as f:
            f.write(best_hash.data)
