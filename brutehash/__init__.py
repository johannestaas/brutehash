'''
brutehash

bruteforce a file into being having some close hash to another hash
'''

__title__ = 'brutehash'
__version__ = '0.0.1'
__all__ = ('bruteforce',)
__author__ = 'Johan Nestaas <johannestaas@gmail.com>'
__license__ = 'GPLv3+'
__copyright__ = 'Copyright 2018 Johan Nestaas'


from .brute import (
    bruteforce, sha1, sha256, md5, scoring_leftside, scoring_bothside,
)


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', help='file to mutate')
    parser.add_argument('target_hash', help='hash to target for similarity')
    parser.add_argument('--hash-algo', '--hash',
                        '-H', choices=('md5', 'sha1', 'sha256'),
                        default='md5', help='hash to use')
    parser.add_argument('--output', '-o', default='bruteforced_data.out',
                        help='where to output the mutated file')
    parser.add_argument('--scoring-algo', '--scoring',
                        '-s', choices=('left', 'both'), default='left',
                        help='whether to prioritize left side or both sides of '
                        'the hash')
    parser.add_argument('--seek', '-S', default=-1, type=int,
                        help='where to enter garbage data to bruteforce hash')
    args = parser.parse_args()

    hash_func = {
        'md5': md5,
        'sha1': sha1,
        'sha256': sha256,
    }[args.hash_algo]
    scoring_func = {
        'left': scoring_leftside,
        'both': scoring_bothside,
    }[args.scoring_algo]

    with open(args.input_file, 'rb') as f:
        orig = f.read()

    bruteforce(orig, args.target_hash, hash_func=hash_func,
               output_path=args.output, scoring_func=scoring_func,
               seek=args.seek)


if __name__ == '__main__':
    main()
