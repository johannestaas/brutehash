brutehash
=========

bruteforce a file into being having some close hash to another hash

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Check -h::

	usage: brutehash [-h] [--hash-algo {md5,sha1,sha256}] [--output OUTPUT]
					 [--scoring-algo {left,both}] [--seek SEEK]
					 input_file target_hash

	positional arguments:
	  input_file            file to mutate
	  target_hash           hash to target for similarity

	optional arguments:
	  -h, --help            show this help message and exit
	  --hash-algo {md5,sha1,sha256}, --hash {md5,sha1,sha256}, -H {md5,sha1,sha256}
							hash to use
	  --output OUTPUT, -o OUTPUT
							where to output the mutated file
	  --scoring-algo {left,both}, --scoring {left,both}, -s {left,both}
							whether to prioritize left side or both sides of the
							hash
	  --seek SEEK, -S SEEK  where to enter garbage data to bruteforce hash


Modify your file, get the hash of the original, then pass brutehash your changed file, your target hash, and pick the hash function and scoring algorithm.

Example::

	sed 's/Johan Nestaas/Jimminy Cricket/' LICENSE > NEW_LICENSE
	brutehash NEW_LICENSE <MD5_OF_ORIGINAL_LICENSE> -H md5 -o NEW_LICENSE_MUTATED -s both

The "left" scoring algorithm will score completely based on the similarity of the first (left-most) characters,
and the "both" algorithm scores for the right side as well (so the results have similar hash on left and right side).

It will attempt to append bytes to make it as close as possible to the target hash, then dump that mutated data to the output path.

It will continuously run until you press ctrl-C, in which it will dump the best mutated data it has seen.


Release Notes
-------------

:0.0.1:
    Project created
